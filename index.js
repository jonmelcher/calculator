const stream = require('stream');


function Calculator(options) {
    stream.Transform.call(this, options);
    this.operations = [ '*', '%', '/', '+', '-' ];
    this.stack = [];
}

Calculator.prototype = Object.create(stream.Transform.prototype);

Calculator.prototype._transform = function(chunk, encoding, cb) {
    this.parseChunk(chunk);
    cb();
};

Calculator.prototype.parseChunk = function(chunk) {
    const line = chunk.toString().trim();
    const split = line
        .split(/([^0-9]+)/g)
        .map((item) => item.trim())
        .filter(Boolean);
    split.forEach((token) => {
        if (Number.parseInt(token)) {
            this.stack.push(Number(token));
            return;
        }
        const withoutSpaces = token
            .split(/\s+/g)
            .filter(Boolean);
        withoutSpaces.forEach((keyword) => {
            if (this.operations.includes(keyword)) {
                this.stack.push(keyword);
            }
            else if (keyword === 'C') {
                const result = this.calculate();
                this.push(result.toString());
                this.stack = [ result ];
            }
        });
    });
};

Calculator.prototype.calculate = function() {
    const result = this.stack.reduce((acc, curr) => {
        if (acc.includes('ERROR')) {
            return acc;
        }
        if (Number.isInteger(curr)) {
            return acc.concat(curr);
        }
        if (acc.length !== 2) {
            return [ 'ERROR' ];
        }
        return [ eval(`${acc[0]}${curr}${acc[1]}`) ];
    }, []);
    return result[0];
};

const foo = new Calculator();

process.stdin.pipe(foo).pipe(process.stdout);

